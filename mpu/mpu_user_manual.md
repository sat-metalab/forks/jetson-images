## MPU-Jetson User manual 

Puara MPU\
Edu Meneses - 2023\
Metalab - Société des Arts Technologiques (SAT)\

- [MPU-Jetson User manual](#mpu-jetson-user-manual)
- [Desktop environment / Windows manager](#desktop-environment--windows-manager)
- [i3wm shortcuts](#i3wm-shortcuts)

## Desktop environment / Windows manager

The MPU-Jetson contains both [GNOME](https://www.gnome.org/) and [i3wm](https://i3wm.org/).
We recomment i3wm to deploy a self-contained system pre-configured to work headlessly.
This configuration will save resources and ensure the graphical interface starts even if there's no monitor connected to the NVIDIA Jetson.
GNOME can be used for development or when users desire a complete desktop environment.

To switch between i3wm and GNOME:

- Log out of the current session using the **Exit i3** shortcut (see list of shortcuts below) or the top-left menu on GNOME
- Select the user (default user is **metalab**)
- click on the gear icon (botton-left of the screen)
- choose the desired environment 

## i3wm shortcuts

Super key: `Alt`

Closing (killing) a program: `Alt+Shift+q`or middle button over a title bar

Start a terminal: `Alt+Return`

Start a program using **dmenu**: `Alt+d`\
Start a program using **Rofi**: `Alt+Shift+d`

Change focus: `Alt+<cursor keys>` (or use _j_, _k_, _l_, or _;_)

Move focused window: `Alt+Shift+<cursor keys>` (or use _j_, _k_, _l_, or _;_)

Split in horizontal orientation: `Alt+h`\
Split in vertical orientation: `Alt+v`

Enter fullscreen mode for the focused window `Alt+f`

Change container layout:\
stacking: `Alt+s`\
tabbed: `Alt+w`\
toggle split: `Alt+e`\

Toggle tiling/floating: `Alt+m``

Switch to the workspace: `Control+`<N`>`, where N can be replaced by the workspace number

Move the focused window to a specific workspace: `ConAlttrol+<N>`, where N can be replaced by the workspace number

Reload i3wm: `Alt+Shift+c`\
Restart i3wm inplace: `Alt+Shift+r`

Exit i3: `Alt+Shift+e`, then choose the desired option: (L)ogout, (R)eboot, (P)oweroff

Resize window (you can also use the mouse for that): `Alt+r`
