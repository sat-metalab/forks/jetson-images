#!/bin/bash

# exit when any command fails
set -e

echo
echo "┌──────────────────────┐"
echo "│ MPU Builder          |"
echo "│ Edu Meneses - 2023   |"
echo "│ Metalab - SAT        |"
echo "│ IDMIL - CIRMMT       |"
echo "└──────────────────────┘"
echo
echo "This script will upgrade your system, "
echo "clone the SAT/Metalab's MPU project, "
echo "generate the building script, and run "
echo "it. Some interaction is required, including "
echo "inserting root password, but most of the "
echo "process will be done automatically."
echo

sudo apt update &&\
sudo apt upgrade -y &&\
sudo apt install -y tmux git liblo-tools nano &&\
sudo mkdir -p /sources && sudo chmod 777 /sources && cd /sources &&\
git clone https://gitlab.com/sat-mtl/distribution/jetson-images.git MPU &&\
cd /sources/MPU &&\
cd /sources/MPU/mpu/scripts &&\
sudo chmod +x building_script.sh rename_mpu.sh change_ipblock.sh &&\
./building_script.sh &&\
sudo chmod +x run_script.sh &&\
./run_script.sh
