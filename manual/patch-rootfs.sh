#! /bin/bash

#
# Author: Badr BADRI © pythops
#

set -e

# Check if board type is specified
if [ ! $JETSON_NANO_BOARD ]; then
	printf "\e[31mJetson nano board type must be specified\e[0m\n"
	exit 1
fi

# Define L4T Driver Package (BSP)
# https://developer.nvidia.com/embedded/downloads
case "$JETSON_NANO_BOARD" in
    jetson-nano-2gb)
        # Jetson Nano, Nano 2GB and TX1
        BSP="https://developer.nvidia.com/embedded/l4t/r32_release_v6.1/t210/jetson-210_linux_r32.6.1_aarch64.tbz2"
        ;;

    jetson-nano)
        # Jetson Nano, Nano 2GB and TX1
        BSP="https://developer.nvidia.com/embedded/l4t/r32_release_v6.1/t210/jetson-210_linux_r32.6.1_aarch64.tbz2"
        ;;

    jetson-xavier-nx)
        # Jetson AGX Xavier Series, Xavier NX and TX2 Series
        BSP="https://developer.nvidia.com/embedded/l4t/r35_release_v1.0/release/jetson_linux_r35.1.0_aarch64.tbz2"
        ;;

    *)
	printf "\e[31mUnknown Jetson nano board L4T Driver Package (BSP)\e[0m\n"
	exit 1
        ;;
esac

# Check if the user is not root
if [ "x$(whoami)" != "xroot" ]; then
        printf "\e[31mThis script requires root privilege\e[0m\n"
        exit 1
fi

# Check for env variables
if [ ! $JETSON_ROOTFS_DIR ] || [ ! $JETSON_BUILD_DIR ]; then
	printf "\e[31mYou need to set the env variables \$JETSON_ROOTFS_DIR and \$JETSON_BUILD_DIR\e[0m\n"
	exit 1
fi

# Check if $JETSON_ROOTFS_DIR if not empty
if [ ! "$(ls -A $JETSON_ROOTFS_DIR)" ]; then
	printf "\e[31mNo rootfs found in $JETSON_ROOTFS_DIR\e[0m\n"
	exit 1
fi

printf "\e[32mBuild the image ...\n"

# Create the build dir if it does not exists
mkdir -p $JETSON_BUILD_DIR

# Download L4T
BSP_FILE=$(basename $BSP)
if [ ! "$(ls -A $BSP_FILE)" ]; then
        printf "\e[32mDownload L4T...       "
        wget -O $BSP_FILE $BSP
        printf "[OK]\n"
fi

if [ ! "$(ls -A $JETSON_BUILD_DIR)" ]; then
        printf "\e[32mExtract L4T...       "
        tar -jxpf $BSP_FILE -C $JETSON_BUILD_DIR
	    rm $JETSON_BUILD_DIR/Linux_for_Tegra/rootfs/README.txt
        printf "[OK]\n"
fi

cp -rp $JETSON_ROOTFS_DIR/*  $JETSON_BUILD_DIR/Linux_for_Tegra/rootfs/ > /dev/null

patch $JETSON_BUILD_DIR/Linux_for_Tegra/nv_tegra/nv-apply-debs.sh < patches/nv-apply-debs.diff

pushd $JETSON_BUILD_DIR/Linux_for_Tegra/ > /dev/null

printf "Apply L4T...        "
./apply_binaries.sh #> /dev/null
printf "[OK]\n"

printf "Update /etc/apt/sources.list.d/nvidia-l4t-apt-source.list...        "
case "$JETSON_NANO_BOARD" in
    jetson-xavier-nx)
        SOC="t194"
        ;;

    jetson-nano)
        SOC="t210"
        ;;

    jetson-nano-2gb)
        SOC="t210"
        ;;

    *)
	printf "\e[31mSOC not defined for board $JETSON_NANO_BOARD \e[0m\n"
	exit 1
        ;;
esac
sed -i "s/<SOC>/$SOC/g" $JETSON_BUILD_DIR/Linux_for_Tegra/rootfs/etc/apt/sources.list.d/nvidia-l4t-apt-source.list
printf "[OK]\n"

printf "Patch tegraflash_internal.py...        "
sed -i "s/output.decode('ascii')/output.decode('utf-8')/g" $JETSON_BUILD_DIR/Linux_for_Tegra/bootloader/tegraflash_internal.py
printf "[OK]\n"

printf "Creating /opt/nvidia/l4t-packages/.nv-l4t-disable-boot-fw-update-in-preinstall to enable upgrade of nvidia packages...        "
mkdir -p $JETSON_BUILD_DIR/Linux_for_Tegra/rootfs/opt/nvidia/l4t-packages/
touch $JETSON_BUILD_DIR/Linux_for_Tegra/rootfs/opt/nvidia/l4t-packages/.nv-l4t-disable-boot-fw-update-in-preinstall
printf "[OK]\n"

printf "\e[32mRootfs patched successfully\n"
