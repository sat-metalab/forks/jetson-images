#!/bin/bash

echo
echo "┌────────────────────────────────┐"
echo "│ Auto Image Generator Script    |"
echo "│ Edu Meneses - 2023             |"
echo "│ Metalab - SAT                  |"
echo "└────────────────────────────────┘"
echo 
echo "This script will run all the steps to generate the image "
echo "for the NVIDIA Jetson Xavier NX single-board computers."
echo "After that you can flash the image and optionally run the "
echo "MPU script to finish the instalation." 
echo
echo "If you want to generate an image for another"
echo "Jetson model, please follow the instructions on:"
echo "https://gitlab.com/sat-mtl/distribution/jetson-images/-/blob/master/Readme.md"
echo

# exit when any command fails
set -e

# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

print_usage() {
  printf "Hostname (-n) and user's (-u) names can also be provided as arguments to this script\n\n"
}

name=''
user=''
password=''

while getopts 'n:u:p:' flag; do
  case "${flag}" in
    n) name="${OPTARG}" ;;
    u) user="${OPTARG}" ;;
    p) password="${OPTARG}" ;;
    *) print_usage
       exit 1 ;;
  esac
done

if [ -z "$name" ]
then
    echo "Error: No hostname was provided."
    echo
    exit 1
else
    export mpu_name=$name
    echo "NVIDIA Jetson hostname: ${mpu_name}"
    echo
fi

if [ -z "$user" ]
then
    echo "Error: No user name was provided."
    echo
    exit 1
else
    export user_name=$user
    echo "NVIDIA Jetson user name: ${user_name}"
    echo
fi

if [ -z "$password" ]
then
    echo "Error: No password was provided."
    echo
    exit 1
else
    export mpu_password=$password
fi

read -r -s -p $'Press enter to continue...\n\n'

echo
echo "Setting variables..."
echo
export SRC_DIR=$(cd .; pwd)
export JETSON_NANO_BOARD=jetson-xavier-nx
export JETSON_BUILD_DIR=${SRC_DIR}/image/build
export JETSON_ROOTFS_DIR=${SRC_DIR}/image/rootfs

echo
echo "Executing create-rootfs script:"
echo
sudo -E ./create-rootfs.sh
pip install --user ansible

echo
echo "Playing jetson playbook:"
echo
pushd ansible
sudo -E $(which ansible-playbook) jetson.yaml
popd

echo
echo "Executing patch-rootfs script:"
echo
sudo -E ./patch-rootfs.sh

echo
echo "Playing sat-metalab playbook:"
echo
cp ${SRC_DIR}/ansible/roles/sat-metalab/defaults/main.yaml ${SRC_DIR}/ansible/roles/sat-metalab/defaults/main.yaml.bak
sudo sed -i -e "s/xavierjetson/${user_name}/" ${SRC_DIR}/ansible/roles/sat-metalab/defaults/main.yaml
sudo sed -i -e "s/Navier Stone/${mpu_password}/" ${SRC_DIR}/ansible/roles/sat-metalab/defaults/main.yaml
pushd ansible
sudo -E $(which ansible-playbook) sat-metalab.yaml
popd
mv ${SRC_DIR}/ansible/roles/sat-metalab/defaults/main.yaml.bak ${SRC_DIR}/ansible/roles/sat-metalab/defaults/main.yaml

echo
echo "Executing create-image script:"
echo
sudo -E ./create-image.sh

echo
echo "Image generation script done!"
echo
