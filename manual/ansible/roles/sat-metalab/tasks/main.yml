---
- name: Create a l4t-packages directory
  ansible.builtin.file:
    path: /opt/nvidia/l4t-packages/
    state: directory
    mode: '0755'

- name: Enable upgrade of nvidia packages
  ansible.builtin.file:
    path: /opt/nvidia/l4t-packages/.nv-l4t-disable-boot-fw-update-in-preinstall
    state: touch
    mode: '0664'

- name: Create /proc/1/ directory to enable installing udisks2
  ansible.builtin.file:
    path: /proc/1
    state: directory
    mode: '0755'

- name: Create /proc/1/root file to enable installing udisks2
  ansible.builtin.file:
    path: /proc/1/root
    state: touch
    mode: '0664'

# NOTE if you need to re-run this playbook, all of the 4 above tasks must be active and create the following files
# /opt/nvidia/l4t-packages/.nv-l4t-disable-boot-fw-update-in-preinstall
# /proc/1/root

- name: Install LXDE desktop environment with lightdm
  apt:
    name: "{{ item }}"
    state: present
  loop:
    - apt-utils
    - dialog
    - lxde
    - lightdm
    - lightdm-gtk-greeter

- name: Create new user
  user:
    name: "{{ new_user.name }}"
    comment: "{{ new_user.comment }}"
    shell: "{{ new_user.shell }}"
    password: "{{ new_user.password | password_hash('sha512') }}"
    create_home: yes
    groups: sudo,video,audio,adm,lpadmin,lightdm
    state: present
# groups sudo,video,audio,adm from Linux_for_Tegra/tools/l4t_create_default_user.sh
# plus lpadmin for usb cameras in livepose

- name: Install packages to add PPAs
  apt:
    name: "{{ item }}"
    state: present
  loop:
    - coreutils
    - software-properties-common
    - wget

- name: Setup SAT-metalab MPAs
  shell: |
    distro=`lsb_release -c | awk -F ' ' '{print $2}'`
    wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-$distro-arm64-jetson/sat-metalab-mpa-keyring.gpg \
    | gpg --dearmor \
    | dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg
    echo deb [ signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-$distro-arm64-jetson/debs/ sat-metalab main \
    | tee /etc/apt/sources.list.d/sat-metalab-mpa.list
    echo deb [ arch=amd64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-datasets/debs/ sat-metalab main \
    | tee /etc/apt/sources.list.d/sat-metalab-mpa-datasets.list

- name: Upgrade apt (backported from focal to bionic to avoid apt update errors)
  shell: |
    distro=`lsb_release -c | awk -F ' ' '{print $2}'`
    if [[ $distro == "bionic" ]]; then
      wget -O /tmp/apt-2.0.6-arm64-ubuntu18.04.tar.gz https://gitlab.com/sat-mtl/metalab/forks/apt/-/package_files/19043786/download
      cd /tmp ;
      tar -xf apt-2.0.6-arm64-ubuntu18.04.tar.gz ;
      dpkg -i libapt-pkg6.0_2.0.6_arm64.deb ;
      dpkg -i apt_2.0.6_arm64.deb ;
      dpkg -i apt-utils_2.0.6_arm64.deb ;
    fi

- name: Upgrade packages
  apt:
    upgrade: dist
    update_cache: yes

- name: Install remote access and diagnosis tools
  apt:
    name: "{{ item }}"
    state: present
  loop:
    - barrier
    - florence
    - gnome-terminal
    - nano

- name: Create a barrier config directory
  ansible.builtin.file:
    path: /home/{{ new_user.name }}/.config/Debauchee
    state: directory
    mode: '0755'

- name: Setup barrier
  template:
    src: Barrier.conf
    dest: /home/{{ new_user.name }}/.config/Debauchee/Barrier.conf
    owner: "{{ new_user.name }}"
    group: "{{ new_user.name }}"
    mode: 0664

- name: Install SAT-metalab tools
  apt:
    name: "{{ item }}"
    state: present
  loop:
    - |
      *livepose*
    - splash-mapper

- name: Configure ldconfig to find libraries in /usr/lib/aarch64-linux-gnu/tegra (bionic only)
  shell: |
    distro=`lsb_release -c | awk -F ' ' '{print $2}'`
    if [[ $distro == "bionic" ]]; then
      echo "/usr/lib/aarch64-linux-gnu/tegra" > /etc/ld.so.conf.d/nvidia-tegra.conf && ldconfig ;
    fi
# DONE in focal by installing package nvidia-l4t-configs

- name: Clean apt cache
  apt:
    clean: yes

- name: Setup autologin
  shell: |
    if [ -f "/usr/share/xsessions/LXDE.desktop" ]; then
      mv "/usr/share/xsessions/LXDE.desktop" "/usr/share/xsessions/ux-LXDE.desktop" ;
    fi
    FILE="/etc/lightdm/lightdm.conf.d/50-nvidia.conf"
    grep -q -F 'user-session=ux-LXDE' "$FILE" || sed -i '1 auser-session=ux-LXDE' "$FILE" ;
    grep -q -F "autologin-user=" "$FILE" || echo "autologin-user="`ls /home` >> "$FILE" ;
    grep -q -F "autologin-user-timeout=0" "$FILE" || echo "autologin-user-timeout=0" >> "$FILE" ;
    echo "/usr/sbin/lightdm" > /etc/X11/default-display-manager ;
# TODO improve `ls /home` assuming one user setup.

- name: Recursively remove l4t-packages directory (needed to enable upgrade of nvidia packages)
  ansible.builtin.file:
    path: /opt/nvidia/l4t-packages
    state: absent

- name: Recursively remove /proc/1 directory (needed to enable installing udisks2)
  ansible.builtin.file:
    path: /proc/1
    state: absent
# NOTE if you need to re-run this playbook, all of the 5 above tasks must be active to delete the following files
# /opt/nvidia/l4t-packages/.nv-l4t-disable-boot-fw-update-in-preinstall
# /proc/1/root
